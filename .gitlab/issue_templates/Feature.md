# Short brief:

What are you thinking of? What is missing? What should we add? What could we change?
Give a short explanation. One or two sentences is enough.

# Description:

Give us more details on what you are thinking of.
What exactly should be done? 
Describe the pros and cons of this feature. 
Why should we add it?

# Sanity check
 [] I have checked previous requests and found no similar request. 
